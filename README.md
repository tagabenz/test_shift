Для запуска в Docker 
`docker compose up --build`

- проект доступен по адресу: 
(http://localhost:8000)
- список ендпоинтов:
(http://localhost:8000/docs)

Для регистрации пользователей и выдачи токена использовался фреймворк **FastAPI Users** (https://fastapi-users.github.io/fastapi-users/latest/)

пользователи для теста:
user@mail.ru
user2@mail.ru
