FROM python:latest

    WORKDIR /app
    COPY app .
    COPY requirements.txt . 
    
    RUN pip install --upgrade pip
    RUN pip install --no-cache-dir -r requirements.txt

    ENTRYPOINT ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8000", "--reload"]