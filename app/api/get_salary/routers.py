from fastapi import APIRouter, Depends
from sqlalchemy import select

from app.auth.users import current_active_user
from app.database import engine
from app.models import Salary, User


router = APIRouter(tags=["salary"])


@router.get("/get_salary")
async def get_salary(user: User = Depends(current_active_user)):
    async with engine.connect() as conn:
        query = (
            select(Salary.value).filter(Salary.user_email==user.email)
        )   

        res = await conn.execute(query)
    
    return {"user salary": f"{res.all()}"}
