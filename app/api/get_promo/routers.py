from fastapi import APIRouter

from sqlalchemy import select

from app.database import engine
from app.models import Promotion

router = APIRouter(tags=["promotion"])


@router.get("/get_promo/{user_email}")
async def get_promo(user_email: str) -> dict:
    async with engine.connect() as conn:
        query = (
            select(Promotion.date).where(Promotion.user_email==user_email)
        )   
        
        res = await conn.execute(query)
    
    return {"Дата повышения": f"{res}"}
