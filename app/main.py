from fastapi import FastAPI, Depends

from contextlib import asynccontextmanager

from sqlalchemy import select

from app.auth.schemas import UserCreate, UserRead, UserUpdate
from app.database import Base, User, create_db_and_tables
# from app.models import Salary
from app.queries import insert_data
from app.auth.users import auth_backend, current_active_user, fastapi_users

from app.api.get_salary.routers import router as salary_router
from app.api.get_promo.routers import router as promo_router

@asynccontextmanager
async def lifespan(app: FastAPI):
    # Not needed if you setup a migration system like Alembic
    # await create_db_and_tables()
    # await insert_data()
    yield
    
    

app = FastAPI(lifespan=lifespan)

app.include_router(
    fastapi_users.get_auth_router(auth_backend), prefix="/auth/jwt", tags=["auth"]
)
app.include_router(
    fastapi_users.get_register_router(UserRead, UserCreate),
    prefix="/auth",
    tags=["auth"],
)
# app.include_router(
#     fastapi_users.get_reset_password_router(),
#     prefix="/auth",
#     tags=["auth"],
# )
# app.include_router(
#     fastapi_users.get_verify_router(UserRead),
#     prefix="/auth",
#     tags=["auth"],
# )
# app.include_router(
#     fastapi_users.get_users_router(UserRead, UserUpdate),
#     prefix="/users",
#     tags=["users"],
# )

app.include_router(salary_router)
app.include_router(promo_router)

@app.get("/")
async def authenticated_route(user: User = Depends(current_active_user)):
    
    return {"message": f"Hello {user.email}!"}

