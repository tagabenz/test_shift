import datetime

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, DeclarativeBase, relationship

from fastapi_users.db import SQLAlchemyBaseUserTableUUID


class Base(DeclarativeBase):
    pass


class User(SQLAlchemyBaseUserTableUUID, Base):
    salary: Mapped["Salary"] = relationship(back_populates="user")
    promotion: Mapped["Promotion"] = relationship(back_populates="user")


class Salary(Base):
    __tablename__ = "salary"
    
    id: Mapped[int] = mapped_column(primary_key=True)
    value: Mapped[int] 
    user_email: Mapped[str] = mapped_column(ForeignKey("user.email", ondelete="CASCADE"),unique=True)
    
    user: Mapped["User"] = relationship(back_populates="salary")


class Promotion(Base):
    __tablename__ = "promotion"

    id: Mapped[int] = mapped_column(primary_key=True)
    date: Mapped[str]
    user_email: Mapped[str] = mapped_column(ForeignKey("user.email", ondelete="CASCADE"),unique=True)
    
    user: Mapped["User"] = relationship(back_populates="promotion")