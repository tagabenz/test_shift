from sqlalchemy import insert, text
import datetime 
from app.database import engine
from app.models import Promotion, Salary


async def insert_data():
    async with engine.connect() as conn:
        stmt_salary = insert(Salary).values(
            [
                {"value":35000, "user_email":"user@mail.ru"},
                {"value":55000, "user_email":"user2@mail.ru"},
            ]
        ) 
        await conn.execute(stmt_salary)

        stmt_promo = """INSERT INTO promotion (date, user_email) VALUES ('2024.7.14','user@mail.ru'), ('2025.10.15','user2@mail.ru');"""
        await conn.execute(text(stmt_promo))
        
        await conn.commit()